<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Illuminate\Support\facades\Hash;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function client_login()
    {
        return view('auth.client_login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function client_register()
    {
        return view('auth.client_register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function client_save(Request $request)
    {
        $request->validate([
            'hospital_name'=>'required',
            'hospital_code'=>'required',
            'email'=>'required|email|unique:clients',
            'password'=>'required|min:5|max:12',
            'address'=>'required',
            'contact_no'=>'required',
            'is_active'=>'required',
        ]);
        
        // insert data into database
        $client = new Client();

        $client->hospital_name = $request->hospital_name;
        $client->hospital_code = $request->hospital_code;
        $client->email = $request->email;
        $client->password = Hash::make($request->password);
        $client->address = $request->address;
        $client->contact_no = $request->contact_no;
        $client->is_active = $request->is_active;

        $save = $client->save();
        if($save){
            return back()->with('success', 'New Client successfully added to database.');
        }else{
            return back()->with('fail', 'Somthing went wrong, please check and try again.');
        }
    }

    // Check if the client exist in the system before login
    public function client_check(Request $request)
    {
        //return $request->input();
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|min:5|max:12',
        ]);

        //match client to the database
        $clientInfo = Client::where('email','=',$request->email)->first();

        if(!$clientInfo){
            return back()->with('fail','We do not recognize your email address.');
        }else{
            //check password
            if(Hash::check($request->password, $clientInfo->password)){
                $request->session()->put('LoggedUser', '$clientInfo->id');
                return redirect('client/dashboard');
            }else{
                return back()->with('fail', "Incorrect password.");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
