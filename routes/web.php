<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/client_login', [AuthController::class, 'client_login'])->name('auth.client_login');
Route::get('/client_register', [AuthController::class, 'client_register'])->name('auth.client_register');
Route::post('/client_save', [AuthController::class, 'client_save'])->name('auth.client_save');
Route::post('/client_check', [AuthController::class, 'client_check'])->name('auth.client_check');

Route::get('/admin/dashboard', [AdminController::class, 'index']);